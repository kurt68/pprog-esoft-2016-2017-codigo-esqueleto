package Dominio;

public class Evento {

	/**
	 * T�tulo identificativo do evento.
	 */
	private string titulo;
	/**
	 * Texto descritivo que oferece uma breve descri��o sobre o que o evento ir� proporcionar.
	 */
	private string textoDescritivo;
	private Candidatura[] candidaturas;
	private FAE[] FAE;
	private Atribuicao[] atribuicoes;
	private string dataInicio;
	private string dataFim;
	private string local;

	public string getTitulo() {
		return this.titulo;
	}

	public void setTitulo(string titulo) {
		this.titulo = titulo;
	}

	public string getTextoDescritivo() {
		return this.textoDescritivo;
	}

	public void setTextoDescritivo(string textoDescritivo) {
		this.textoDescritivo = textoDescritivo;
	}

	public Candidatura[] getCandidaturas() {
		return this.candidaturas;
	}

	public FAE[] getFAE() {
		return this.FAE;
	}

	public Atribuicao[] getAtribuicoes() {
		return this.atribuicoes;
	}

	public void setDataInicio(string dataInicio) {
		this.dataInicio = dataInicio;
	}

	public void setDataFim(string dataFim) {
		this.dataFim = dataFim;
	}

	public void setLocal(string local) {
		this.local = local;
	}

	/**
	 * Verifica se o FAE criado j� foi anteriormente "vinculado" ao evento em quest�o.
	 * @param fae FAE a ser validado, sendo verificado se este j� se encontra vinculado ao evento em quest�o.
	 */
	public boolean validaFAE(FAE fae) {
		// TODO - implement Evento.validaFAE
		throw new UnsupportedOperationException();
	}

	/**
	 * M�todo que vai criar uma inst�ncia de FAE ao qual ficar� associado um Utilizador (o que foi indicado pelo Organizador)
	 */
	public FAE novoFAE() {
		// TODO - implement Evento.novoFAE
		throw new UnsupportedOperationException();
	}

	/**
	 * Regista / adiciona os FAE escolhidos ao evento escolhido pelo Organizador, ap�s este confirmar a sele��o.
	 * @param FAENovos
	 */
	public void addFAE(FAE[] FAENovos) {
		// TODO - implement Evento.addFAE
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param atribuicao
	 */
	public void addAtribuicao(Atribuicao atribuicao) {
		// TODO - implement Evento.addAtribuicao
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param candidaturasEscolhidas
	 */
	public void eliminarCandidaturasEscolhidas(Candidatura[] candidaturasEscolhidas) {
		// TODO - implement Evento.eliminarCandidaturasEscolhidas
		throw new UnsupportedOperationException();
	}

	public void createCandidatura() {
		// TODO - implement Evento.createCandidatura
		throw new UnsupportedOperationException();
	}

	public void validaCandidatura() {
		// TODO - implement Evento.validaCandidatura
		throw new UnsupportedOperationException();
	}

	public void createAtribuicao() {
		// TODO - implement Evento.createAtribuicao
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param candidatura
	 */
	public void addCandidatura(Candidatura candidatura) {
		// TODO - implement Evento.addCandidatura
		throw new UnsupportedOperationException();
	}

	public Candidatura[] getCandidaturasPorAvaliar() {
		// TODO - implement Evento.getCandidaturasPorAvaliar
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param candidaturaEscolhida
	 */
	public Atribuicao getAtribuicao(Candidatura candidaturaEscolhida) {
		// TODO - implement Evento.getAtribuicao
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param util
	 */
	public void addOrganizador(Utilizador util) {
		// TODO - implement Evento.addOrganizador
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param organizador
	 */
	public void addOrganizador(Organizador organizador) {
		// TODO - implement Evento.addOrganizador
		throw new UnsupportedOperationException();
	}

	public void valida() {
		// TODO - implement Evento.valida
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param fae
	 */
	public void registaFAE(FAE fae) {
		// TODO - implement Evento.registaFAE
		throw new UnsupportedOperationException();
	}

	public void validaAvaliacao() {
		// TODO - implement Evento.validaAvaliacao
		throw new UnsupportedOperationException();
	}

}