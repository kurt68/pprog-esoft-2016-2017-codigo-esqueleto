package Dominio;

public class Utilizador {

	private boolean confirmado;
	private string nome;
	private string email;
	private string password;
	private string username;

	public boolean isConfirmado() {
		return this.confirmado;
	}

	public void setConfirmado(boolean confirmado) {
		this.confirmado = confirmado;
	}

	public void setNome(string nome) {
		this.nome = nome;
	}

	public void setEmail(string email) {
		this.email = email;
	}

	public void setPassword(string password) {
		this.password = password;
	}

	public void setUsername(string username) {
		this.username = username;
	}

	public void valida() {
		// TODO - implement Utilizador.valida
		throw new UnsupportedOperationException();
	}

}