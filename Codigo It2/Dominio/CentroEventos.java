package Dominio;

public class CentroEventos {

	/**
	 * Array do tipo Evento que cont�m refer�ncias para os diversos eventos ao encargo do Centro de Eventos.
	 */
	private Evento[] eventos;

	public Evento[] getEventos() {
		return this.eventos;
	}

	/**
	 * M�todo que obt�m os eventos organizados pelo Organizador passado como par�metro. (N�o � um "get" no sentido que estamos habituados e que retorna o valor/refer�ncia de uma dada vari�vel!).
	 */
	public Evento[] getEventosOrganizador() {
		// TODO - implement CentroEventos.getEventosOrganizador
		throw new UnsupportedOperationException();
	}

	/**
	 * Recebe um email e retorna um Utilizador associado a esse email.
	 * @param email Email fornecido pelo organizador do FAE a ser atribu�do ao evento.
	 */
	public Utilizador giveUtilizador(string email) {
		// TODO - implement CentroEventos.giveUtilizador
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param utilizadoresEscolhidos
	 */
	public void confirmaUtilizadoresEscolhidos(Utilizador[] utilizadoresEscolhidos) {
		// TODO - implement CentroEventos.confirmaUtilizadoresEscolhidos
		throw new UnsupportedOperationException();
	}

	public Utilizador[] getUtilizadoresNaoRegistados() {
		// TODO - implement CentroEventos.getUtilizadoresNaoRegistados
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param email
	 */
	public boolean validaEmail(string email) {
		// TODO - implement CentroEventos.validaEmail
		throw new UnsupportedOperationException();
	}

	public Evento[] getEventosFAE() {
		// TODO - implement CentroEventos.getEventosFAE
		throw new UnsupportedOperationException();
	}

	public Evento novoEvento() {
		// TODO - implement CentroEventos.novoEvento
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param evento
	 */
	public void registaEvento(Evento evento) {
		// TODO - implement CentroEventos.registaEvento
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param evento
	 */
	public void validaEvento(Evento evento) {
		// TODO - implement CentroEventos.validaEvento
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param evento
	 */
	public void addEvento(Evento evento) {
		// TODO - implement CentroEventos.addEvento
		throw new UnsupportedOperationException();
	}

	public Utilizador novoUtilizador() {
		// TODO - implement CentroEventos.novoUtilizador
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param utilizador
	 */
	public void validaUtilizador(Utilizador utilizador) {
		// TODO - implement CentroEventos.validaUtilizador
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param utilizador
	 */
	public void registaUtilizador(Utilizador utilizador) {
		// TODO - implement CentroEventos.registaUtilizador
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param utilizador
	 */
	public void addUtilizador(Utilizador utilizador) {
		// TODO - implement CentroEventos.addUtilizador
		throw new UnsupportedOperationException();
	}

}