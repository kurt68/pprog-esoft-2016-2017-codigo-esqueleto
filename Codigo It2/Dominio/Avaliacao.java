package Dominio;

public class Avaliacao {

	private boolean aceite;
	private string justificacao;

	public void setAceite(boolean aceite) {
		this.aceite = aceite;
	}

	public void setJustificacao(string justificacao) {
		this.justificacao = justificacao;
	}

}