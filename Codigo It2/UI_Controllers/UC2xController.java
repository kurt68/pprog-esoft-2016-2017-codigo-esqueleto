package UI_Controllers;

import Dominio.*;

/**
 * Classe Pure Fabrication que recebe ou trata de opera��es do sistema. Representa o caso de uso no qual o evento ocorre. Tem como principal fun��o a delega��o de a��es sobre outros objetos.
 */
public class UC2xController {

	private Evento eventoEscolhido;
	private Evento[] eventosOrganizador;

	public Evento getEventoEscolhido() {
		return this.eventoEscolhido;
	}

	public void setEventoEscolhido(Evento eventoEscolhido) {
		this.eventoEscolhido = eventoEscolhido;
	}

	/**
	 * M�todo que obt�m os eventos organizados pelo Organizador passado como par�metro. (N�o � um "get" no sentido que estamos habituados e que retorna o valor/refer�ncia de uma dada vari�vel!).
	 */
	public Evento[] getEventosOrganizador() {
		return this.eventosOrganizador;
	}

	/**
	 * 
	 * @param email Email do utilizador a ser escolhido como FAE do evento escolhido pelo utilizador anteriormente.
	 */
	public FAE novoFAE(string email) {
		// TODO - implement UC2xController.novoFAE
		throw new UnsupportedOperationException();
	}

	public void addFAE() {
		// TODO - implement UC2xController.addFAE
		throw new UnsupportedOperationException();
	}

}